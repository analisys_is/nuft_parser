from bs4 import BeautifulSoup
from io import StringIO
import json
import argparse

# Default formats matcher : Л -> 0, ПрС -> 2, Лаб -> 3
format_dict = {
  'Л'  : 0,
  'ПрС': 2,
  'Лаб': 3
}

degree_dict = {
  'Доцент'   : 2,
  'Професор' : 3,
  'Асистент' : 4,
}

def parse_days(days):
  digital_days = []
  for day in days:
    if day.contents[0] == 'Понеділок':
      digital_days.append(1)
    if day.contents[0] == 'Вівторок':
      digital_days.append(2)
    if day.contents[0] == 'Середа':
      digital_days.append(3)
    if day.contents[0] == 'Четвер':
      digital_days.append(4)
    if day.contents[0] == 'П\'ятниця':
      digital_days.append(5)
  return digital_days

def parse_room_data(room):
  room_data = {}
  room_data['housing'] = room[0]
  room_data['room_floor'] = room[2:3]
  room_data['room_num'] = room[-3:]
  return room_data

def extractDegree(title):
  for key in degree_dict.keys():
    if title.find(key) != -1:
      return degree_dict[key] 

"""

  Example : Органічні харчові продукти спеціального призначення. Модуль 2 (Л) Дорохович В.В. Професор  Б-301
  Example : Статистичні задачі у харчових технологіях (Л) Мисюра Т.Г. Доцент А-210
  Step 1 :  ^_________________________________________^
  Step 2 :                                            ^_^
  Step 3 :                                              ^___________^
  Step 4 :                                                                   ^___^ 

"""
def parse_lesson_name(lesson_name):
  features = {}
  # Step 1 : extract lesson name (all tokens till first open bracket : '(')
  bracket_idx = lesson_name.index('(')
  features['name'] = lesson_name[0:bracket_idx]

  # Step 2 : extract lesson format : Л -> 0, ПрС -> 2, Лаб -> 3
  bracket_idx += 1
  close_bracket_idx = lesson_name.index(')')
  features['format'] = format_dict[lesson_name[bracket_idx:close_bracket_idx]]

  # Step 3 : extract professor name. Exhausting process =/

  teacher = {}
  close_bracket_idx += 1
  # We gonna find first dot symbol, make lookup on 3 symbols forward and crop substring.
  """
  Description.
  Example    :   Модуль 2 (Л) Дорохович В.В. Професор  Б-301
  1st action :              ^                --- crop substring from @close_bracket_idx position.
  2nd action :                           ^   --- get first dot
  3rd action :                           __^ --- make lookup; we expect only 3 sybmols afterwords.
  4th action :               ^_____________^ --- extract name by calling substring 
  """ 
  professor_name = lesson_name[close_bracket_idx:]  # 1st action
  name_end_idx = professor_name.index('.') + 3      # 2nd and 3rd action
  professor_name = professor_name[0:name_end_idx]   # 4th action.
  teacher['last_name'] = professor_name
  teacher['degree'] = extractDegree(lesson_name)
  features['teachers'] = [teacher]

  # Step 4 : extract building and room name. As a rule, it is last 5 symbols.
  features['room_data'] = parse_room_data(lesson_name[-6:-1])
  return features


""" 
  README!!!
  I have some explanation for you, dear @Reader: 
  Imagine, that you have 3 days scheduled, each day has 6 slots for lesson.
    Lesson [day 1, lesson 1]
    Lesson [day 1, lesson 2]
    ... 
    Lesson [day 1, lesson 6]
    Lesson [day 2, lesson 1]
              ^^^ You should increase this somehow.
              And set @lesson_idx as 1, also.
  
  For this operation I created condition at line 118. 
"""
def parse_group_schedule(soup):
  group_schedule = []
  # Observation : each day in schedule marked with 'small' tag
  days = parse_days(soup.findAll('small'))
  # Observation : group can have from 3 to 6 lessons per day and from 2 to 5 days in university.
  lessons = len(soup.find('td').findAllNext(text=lambda x : len(x)==1 and x != " "))
  
  lessons_count = lessons / len(days)
  lesson_idx = 0
  day_idx = -1

  for lesson in soup.findAll('tr'):
    lesson_idx += 1
    if lesson_idx % lessons_count == 1:
      day_idx += 1
      lesson_idx = 1
    if len(lesson.contents[2].contents[0]) > 1:
      features = parse_lesson_name(lesson.contents[2].contents[0])
      features['lesson_time'] = lesson_idx
      features['short_name'] = ""
      features['day'] = days[day_idx] 
      group_schedule.append(features)
  return group_schedule

if __name__ == "__main__":
  parser = argparse.ArgumentParser(description='Process some integers.')  
  parser.add_argument("filename", type=str, help="file to parse")
  args = parser.parse_args()

  f = open(args.filename, 'r', encoding='cp1251')
  result_file = open('result.json', 'w+', encoding='utf-8')
  soup = BeautifulSoup(f.read(), "html")
  res = parse_group_schedule(soup)
  """
  Attention! For correct JSON decoding you should use StringIO !!!
  Like this :  
  """
  # test = StringIO(json.dumps(res[0]))
  # print(json.load(test))
  result_file.write(json.dumps({'lessons': res}))
  print('Done.')