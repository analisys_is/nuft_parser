# LP Parser

Parses National University of Food Technologies timetable request.

## Requirements

- Python 3
- Beautiful Soup 4 (`bs4`)
- `requests`

See more in `requirements.txt`

## How to use

To start script execute the current package like following

```sh
python parser.py [FILE_PATH]
```